# FooBank

FooBank is a fictional financial services company, based in Athens,
Greece, that offers loans for expensive purchases.

### Technologies 

- Elixir
- Postgres

### How do I run it?

- After installing Elixir 1.9 and postgres, run the following commands:

```
mix deps.get
mix ecto.create
mix ecto.migrate
mix run --no-halt

curl --request POST \
  --url http://localhost:4001/loan_applications \
  --header 'content-type: application/json' \
  --data '{
	"name": "Marcus Vieira",
	"phone_number": "+5511976335858",
	"email": "marcusvieira@me.com",
	"requested_loan_amount": 1300
}'

curl --request GET --url http://localhost:4001/loan_applications

```

You can also run `dialyzer` using `mix dialyzer`


