### Assumptions

- A "user" is defined by the combination of `name` + `email` + `phone_number`
- The format of the `phone_number` is `+(country_code)(phone_number)`
- `requested_loan_amount` and `rate` should be stored as floats
- A user can apply multiple times for a loan
- The following rule
> 1. If a loan application has a lower “Requested loan amount” than any
>    of the previous loan applications, then the loan is rejected.
  is being applied as follows:
    - The first ever loan that a user applies to will always be accepted;
    - The following loan applications must have a `requested_loan_amount` greater than the previous ones.

