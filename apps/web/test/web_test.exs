defmodule WebTest do
  use ExUnit.Case, async: true
  use Plug.Test
  use Mimic

  @opts Web.Router.init([])

  describe "POST /loan_applications" do
    test "creates a loan application with valid attributes" do
      expect(Loan, :apply_for_loan, fn _ ->
        {:ok,
         %{
           name: "marcus vieira",
           requested_loan_amount: 1000.00,
           phone_number: "+5511976335858",
           email: "marcusvieira@me.com",
           status: "ACCEPTED",
           rate: 5.00
         }}
      end)

      attributes = %{
        name: "Marcus Vieira",
        requested_loan_amount: 1000.00,
        phone_number: "+5511976335858",
        email: "marcusvieira@me.com"
      }

      conn = conn(:post, "/loan_applications", Jason.encode!(attributes))
      conn = Web.Router.call(conn, @opts)

      assert conn.state == :sent
      assert conn.status == 201

      assert %{
               "email" => "marcusvieira@me.com",
               "name" => "marcus vieira",
               "phone_number" => "+5511976335858",
               "rate" => 5.0,
               "requested_loan_amount" => 1.0e3,
               "status" => "ACCEPTED"
             } = Jason.decode!(conn.resp_body)
    end

    test "does not create a loan application with invalid attributes" do
      expect(Loan, :apply_for_loan, fn _ ->
        {:error, %{errors: [requested_loan_amount: {"can't be blank", []}]}}
      end)

      attributes = %{
        name: "Marcus Vieira",
        requested_loan_amount: nil,
        phone_number: "+5511976335858",
        email: "marcusvieira@me.com"
      }

      conn = conn(:post, "/loan_applications", Jason.encode!(attributes))
      conn = Web.Router.call(conn, @opts)

      assert conn.state == :sent
      assert conn.status == 422

      assert %{"errors" => [%{"requested_loan_amount" => "can't be blank"}]} =
               Jason.decode!(conn.resp_body)
    end
  end

  describe "GET /loan_applications" do
    test "list all loan applications" do
      expect(Loan, :list_loan_applications, fn ->
        [
          %{
            name: "marcus vieira",
            requested_loan_amount: 1000.00,
            phone_number: "+5511976335858",
            email: "marcusvieira@me.com",
            status: "ACCEPTED",
            rate: 5.00
          },
          %{
            name: "marcus vieira",
            requested_loan_amount: 800.00,
            phone_number: "+5511976335858",
            email: "marcusvieira@me.com",
            status: "REJECTED",
            rate: nil
          }
        ]
      end)

      conn = conn(:get, "/loan_applications")
      conn = Web.Router.call(conn, @opts)

      assert conn.state == :sent
      assert conn.status == 200

      assert [
               %{
                 "email" => "marcusvieira@me.com",
                 "name" => "marcus vieira",
                 "phone_number" => "+5511976335858",
                 "rate" => nil,
                 "requested_loan_amount" => 800.0,
                 "status" => "REJECTED"
               },
               %{
                 "email" => "marcusvieira@me.com",
                 "name" => "marcus vieira",
                 "phone_number" => "+5511976335858",
                 "rate" => 5.0,
                 "requested_loan_amount" => 1.0e3,
                 "status" => "ACCEPTED"
               }
             ] = Jason.decode!(conn.resp_body)
    end
  end
end
