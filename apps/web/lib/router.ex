defmodule Web.Router do
  use Plug.Router

  alias Web.ErrorView
  alias Web.LoanApplicationView

  plug(Plug.Logger)
  plug(:match)
  plug(Plug.Parsers, parsers: [:json], json_decoder: Jason)
  plug(:dispatch)

  post "/loan_applications" do
    attributes = atomize(conn.body_params)

    case Loan.apply_for_loan(attributes) do
      {:ok, loan_application} ->
        send_resp(
          conn,
          201,
          LoanApplicationView.render("loan_application.json", loan_application)
        )

      {:error, changeset} ->
        send_resp(conn, 422, ErrorView.render("error.json", changeset))
    end
  end

  get "/loan_applications" do
    loan_applications = Loan.list_loan_applications()

    send_resp(conn, 200, LoanApplicationView.render("loan_application.json", loan_applications))
  end

  match _ do
    send_resp(conn, 404, "Route Not Found")
  end

  defp atomize(map) do
    for {key, val} <- map, into: %{}, do: {String.to_atom(key), val}
  end
end
