defmodule Web.ErrorView do
  def render("error.json", %{error: {field, detail}}) do
    %{errors: [%{field => render_detail(detail)}]} |> Jason.encode!()
  end

  def render("error.json", %{error: message}) do
    %{errors: message} |> Jason.encode!()
  end

  def render("error.json", %{errors: errors}) do
    errors =
      Enum.map(errors, fn {field, detail} ->
        %{field => render_detail(detail)}
      end)

    %{errors: errors} |> Jason.encode!()
  end

  defp render_detail({message, values}) do
    translated_error = translate_error({message, values})

    Enum.reduce(values, translated_error, fn {k, v}, acc ->
      String.replace(acc, "%{#{k}}", to_string(v))
    end)
  end

  defp render_detail(message) do
    translate_error({message, []})
  end

  def translate_error({msg, _opts}), do: "#{msg}"
end
