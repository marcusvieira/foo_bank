defmodule Web.LoanApplicationView do
  require Logger

  def render("loan_application.json", loan_applications) when is_list(loan_applications) do
    Enum.reduce(loan_applications, [], fn loan_application, acc ->
      [
        %{
          name: loan_application.name,
          phone_number: loan_application.phone_number,
          email: loan_application.email,
          requested_loan_amount: loan_application.requested_loan_amount,
          status: loan_application.status,
          rate: loan_application.rate
        }
        | acc
      ]
    end)
    |> Jason.encode!()
  end

  def render("loan_application.json", loan_application) when is_map(loan_application) do
    %{
      name: loan_application.name,
      phone_number: loan_application.phone_number,
      email: loan_application.email,
      requested_loan_amount: loan_application.requested_loan_amount,
      status: loan_application.status,
      rate: loan_application.rate
    }
    |> Jason.encode!()
  end
end
