defmodule Persistence.DataCase do
  @moduledoc false

  use ExUnit.CaseTemplate

  using do
    quote do
      import Persistence.DataCase
    end
  end

  alias Ecto.Adapters.SQL.Sandbox
  alias Persistence.Repo

  setup tags do
    :ok = Sandbox.checkout(Repo)

    unless tags[:async] do
      Sandbox.mode(Repo, {:shared, self()})
    end

    :ok
  end
end
