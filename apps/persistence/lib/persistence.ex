defmodule Persistence do
  @moduledoc """
  Documentation for Persistence.
  """

  defdelegate insert(changeset), to: Persistence.Repo
  defdelegate all(schema), to: Persistence.Repo
end
