import Config

config :logger, level: :error

config :persistence, Persistence.Repo,
  database: "foo_bank_test",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
