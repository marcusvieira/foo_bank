import Config

config :persistence, Persistence.Repo,
  database: "foo_bank_dev",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"
