defmodule Persistence.Repo.Migrations.CreateLoanApplications do
  use Ecto.Migration

  def change do
    create table(:loan_applications) do
      add(:name, :string, null: false)
      add(:email, :string, null: false)
      add(:phone_number, :string, null: false)
      add(:status, :string, null: false)
      add(:requested_loan_amount, :float, null: false)
      add(:rate, :float)
    end

    create index(:loan_applications, [:name, :phone_number, :email])
  end
end
