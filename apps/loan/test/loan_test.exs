defmodule LoanTest do
  use Persistence.DataCase

  alias Loan.Schema.LoanApplication

  describe "apply_for_loan/1" do
    test "accepts a loan_application when all attributes are valid" do
      loan_application = %{
        name: "Marcus Vieira",
        requested_loan_amount: 1000.00,
        phone_number: "+5511976335858",
        email: "marcusvieira@me.com"
      }

      {:ok, loan_application} = Loan.apply_for_loan(loan_application)

      assert %LoanApplication{
               name: "marcus vieira",
               requested_loan_amount: 1000.00,
               phone_number: "+5511976335858",
               email: "marcusvieira@me.com",
               status: "ACCEPTED"
             } = loan_application

      assert is_float(loan_application.rate)
      assert trunc(loan_application.rate) in 4..12
    end

    test "rejects a loan_application when its amount is lower then the previous ones" do
      loan_application = %{
        name: "Marcus Vieira",
        requested_loan_amount: 1000.00,
        phone_number: "+5511976335858",
        email: "marcusvieira@me.com"
      }

      assert {:ok, %LoanApplication{status: "ACCEPTED"}} = Loan.apply_for_loan(loan_application)

      loan_application = %{
        name: "Marcus Vieira",
        requested_loan_amount: 500.00,
        phone_number: "+5511976335858",
        email: "marcusvieira@me.com"
      }

      assert {:ok, %LoanApplication{status: "REJECTED"}} = Loan.apply_for_loan(loan_application)
    end

    test "does not accept a loan_application when attributes are invalid" do
      loan_application = %{
        name: "Marcus Vieira",
        requested_loan_amount: 1000.00,
        phone_number: "+5511976335858",
        email: "marcusvieirame.com"
      }

      {:error, %{valid?: false}} = Loan.apply_for_loan(loan_application)
    end
  end

  describe "list_loan_applications/0" do
    test "returns a list of all loan_applications" do
      loan_application = %{
        name: "Marcus Vieira",
        requested_loan_amount: 500.00,
        phone_number: "+5511976335858",
        email: "marcusvieira@me.com"
      }

      {:ok, loan_application} = Loan.apply_for_loan(loan_application)
      assert [loan_application] == Loan.list_loan_applications()
    end
  end
end
