defmodule Loan.LoanRepo do
  @moduledoc false

  @persistence Application.get_env(:loan, :persistence)

  import Ecto.Query, only: [from: 2]

  alias Loan.Schema.LoanApplication

  @spec insert(map) :: {:ok, %LoanApplication{}} | {:error, %Ecto.Changeset{}}
  def insert(changeset), do: @persistence.insert(changeset)

  @spec list_previous_loans(map) :: list(%LoanApplication{}) | []
  def list_previous_loans(
        %{name: name, phone_number: phone_number, email: email} = _loan_application
      ) do
    filters = [
      name: normalize_string(name),
      email: normalize_string(email),
      phone_number: phone_number
    ]

    from(la in LoanApplication, where: ^filters, order_by: [desc: la.requested_loan_amount])
    |> @persistence.all()
  end

  @spec list_all_loans() :: list(%LoanApplication{}) | []
  def list_all_loans, do: @persistence.all(LoanApplication)

  defp normalize_string(value), do: String.downcase(value)
end
