defmodule Loan do
  @moduledoc """
  Documentation for Loan.
  """

  @type loan_application :: %{
    name: String.t(),
    requested_loan_amount: float(),
    phone_number: String.t(),
    email: String.t()
  }

  alias Loan.{ApplyLoan, LoanRepo}

  defdelegate apply_for_loan(loan_application), to: ApplyLoan, as: :process
  defdelegate list_loan_applications(), to: LoanRepo, as: :list_all_loans
end
