defmodule Loan.ApplyLoan do
  @moduledoc false

  import Ecto.Changeset

  alias Loan.LoanRepo
  alias Loan.Schema.LoanApplication

  @spec process(Loan.loan_application()) :: {:ok, %LoanApplication{}} | {:error, %Ecto.Changeset{}}
  def process(loan_application) do
    case LoanRepo.list_previous_loans(loan_application) do
      [] ->
        create_accepted_loan_application(loan_application)

      loans ->
        case requested_amount_greater_than_previous?(loans, loan_application) do
          true ->
            create_accepted_loan_application(loan_application)

          false ->
            create_rejected_loan_application(loan_application)
        end
    end
  end

  defp requested_amount_greater_than_previous?([loan | _], loan_application),
    do: loan_application.requested_loan_amount > loan.requested_loan_amount

  defp create_accepted_loan_application(loan_application) do
    loan_application
    |> accepted_loan_application_changeset()
    |> LoanRepo.insert()
  end

  defp create_rejected_loan_application(loan_application) do
    loan_application
    |> rejected_loan_application_changeset()
    |> LoanRepo.insert()
  end

  defp accepted_loan_application_changeset(attributes) do
    %LoanApplication{}
    |> cast(attributes, [:name, :email, :phone_number, :requested_loan_amount])
    |> put_change(:status, "ACCEPTED")
    |> put_change(:rate, calculate_rate(attributes.requested_loan_amount))
    |> validate_format(:email, ~r/@/)
    |> update_change(:name, &normalize_string(&1))
    |> update_change(:email, &normalize_string(&1))
    |> validate_required([:name, :email, :phone_number, :requested_loan_amount, :status])
  end

  defp rejected_loan_application_changeset(attributes) do
    %LoanApplication{}
    |> cast(attributes, [:name, :email, :phone_number, :requested_loan_amount])
    |> put_change(:status, "REJECTED")
    |> validate_format(:email, ~r/@/)
    |> update_change(:name, &normalize_string(&1))
    |> update_change(:email, &normalize_string(&1))
    |> validate_required([:name, :email, :phone_number, :requested_loan_amount, :status])
  end

  defp calculate_rate(nil), do: nil
  defp calculate_rate(requested_loan_amount) do
    case prime?(trunc(requested_loan_amount)) do
      true ->
        9.99

      false ->
        generate_rate()
    end
  end

  defp generate_rate, do: Enum.random(4..12) / 1

  def prime?(1), do: false
  def prime?(number) when number in [2, 3], do: true
  def prime?(number), do: prime?(number, 2)
  def prime?(number, number), do: true

  def prime?(number, m) do
    case rem(number, m) == 0 do
      false -> prime?(number, m + 1)
      true -> false
    end
  end

  defp normalize_string(value), do: String.downcase(value)
end
