defmodule Loan.Schema.LoanApplication do
  use Ecto.Schema

  schema "loan_applications" do
    field(:name, :string)
    field(:email, :string)
    field(:phone_number, :string)
    field(:status, :string)
    field(:rate, :float)
    field(:requested_loan_amount, :float)
  end
end
