### Names

- since I'm not totally aware of the domain (Loaning), I went with rather verbose names, but with more information these could certainly be better (by which I mean, they would properly reflect the business language)

### Testing

- The `persistence` module could be mocked if its computations were too demanding
- Integration tests (Web -> Loan, although i would argue for mocking the integration)

### Implementation

- A notion of authorization or authentication when it comes to both APIs;
- The `Loan` module will throw an error in case it doesn't receive the necessary attributes;
- The logic inside router could be moved to plugs or controllers;
- The `views` could be turned into simple serializers, and not introduce this concept of views;
- Returning a changeset might couple our components to Ecto - and that's not ideal;
- The `prime?` functions could be extracted to a different module



